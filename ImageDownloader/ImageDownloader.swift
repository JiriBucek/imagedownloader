//
//  ImageDownloader.swift
//  ImageDownloader
//
//  Created by Jiri Bucek on 02.04.19.
//  Copyright © 2019 JiriB. All rights reserved.
//

import UIKit
import Foundation
import CommonCrypto


class ImageDownloader{
    private weak var VC: SignInViewController?
    
    init( VC: SignInViewController) {
        self.VC = VC
    }

    // Public
    
    func downloadImage(login: String, password: String){
        let url = URL(string: "https://mobility.cleverlance.com/download/bootcamp/image.php")!
        var request = URLRequest(url: url)
        let encryptedPassword = encryptIntoSHA1(password)
        let parameterString = "username=\(login)"
        request.httpMethod = "Post"
        request.httpBody = parameterString.data(using: .utf8)
        request.allHTTPHeaderFields = [
            "authorization": "\(encryptedPassword)",
        ]
        
        URLSession.shared.dataTask(with: request) {
            (data, response, error) in
            
            if let response = response as? HTTPURLResponse{
                switch response.statusCode{
                case 401:
                    self.VC?.displayMessage(userMessage: "Wrong sign in details")
    
                case 200:
                    if let image = self.decodeImage(with: data){
                        OperationQueue.main.addOperation {
                            self.VC?.displayImage(image: image)
                        }
                    }
                    
                default:
                    self.VC?.displayMessage(userMessage: "Something went wrong.")
                    print("Request error: \(String(describing: error?.localizedDescription))\n")
                }
            }
        }.resume()
    }
    
    // Private
    
    private func encryptIntoSHA1(_ text : String) -> String {
        let data = Data(text.utf8)
        var digest = [UInt8](repeating: 0, count:Int(CC_SHA1_DIGEST_LENGTH))
        data.withUnsafeBytes {
            _ = CC_SHA1($0, CC_LONG(data.count), &digest)
        }
        let hexBytes = digest.map { String(format: "%02hhx", $0) }
        return hexBytes.joined()
    }
    
    private func decodeImage(with data: Data?) -> UIImage?{
        var image: UIImage?
        
        if let data = data{
            do{
                let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String : String]
                if let imageString = json["image"]{
                    if let imageData = Data(base64Encoded: imageString, options: []){
                        if let decodedImage = UIImage(data: imageData){
                            image = decodedImage
                        }
                    }
                }
            }catch let decodeError as NSError{
                print("Decoder error: \(decodeError.localizedDescription)\n")
            }
        }else{
            return nil
        }
        return image
    }
    
}
