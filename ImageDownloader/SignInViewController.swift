//
//  ViewController.swift
//  ImageDownloader
//
//  Created by Boocha on 02.04.19.
//  Copyright © 2019 JiriB. All rights reserved.
//

import UIKit

class SignInViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    @IBOutlet weak var signInButton: UIButton!
    
    @IBOutlet weak var imageView: UIImageView!
    
    @IBOutlet weak var spinnerView: UIActivityIndicatorView!
    
    @IBAction func signInButtonPressed(_ sender: Any) {
        dismissKeyboard()
        startDownload()
    }
    
    var imageDownloader: ImageDownloader?
    var webReachability: WebReachability?
    var downloading = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageDownloader = ImageDownloader(VC: self)
        webReachability = WebReachability()
        spinnerView.hidesWhenStopped = true
        setLayout()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func displayImage(image: UIImage){
        imageView.alpha = 0
        setSpinner(isSpinning: false)
        imageView.image = image
        
        UIView.animate(withDuration: 1.5, delay: 0, options: UIView.AnimationOptions.curveEaseIn, animations:  {
            self.imageView.alpha = 1.0
        })

    }
    
    func startDownload(){
        if let connected = webReachability?.isConnectedToNetwork(){
            if connected{
                if let login = userNameTextField.text, let password = passwordTextField.text{
                    if login.isEmpty || password.isEmpty{
                        displayMessage(userMessage: "Fill in the log in data.")
                    }else{
                        imageView.image = nil
                        imageDownloader?.downloadImage(login: login, password: password)
                        setSpinner(isSpinning: true)
                    }
                }
            }else{
                displayMessage(userMessage: "You are not connected to the internet.")
            }
        }
    }
    
    func displayMessage(userMessage: String) -> Void {
        OperationQueue.main.addOperation
            {
                self.setSpinner(isSpinning: false)
                let alertController = UIAlertController(title: "Error", message: userMessage, preferredStyle: .alert)
                alertController.view.tintColor = UIColor(red: 255/255, green: 161/255, blue: 0, alpha: 1)
                let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                    DispatchQueue.main.async
                        {
                            alertController.dismiss(animated: true, completion: nil)
                    }
                }
                alertController.addAction(OKAction)
                self.present(alertController, animated: true, completion:nil)
        }
    }
    
    func setLayout(){
        let height = userNameTextField.frame.height / 2
        
        userNameTextField.layer.cornerRadius = height
        userNameTextField.clipsToBounds = true
        
        passwordTextField.layer.cornerRadius = height
        passwordTextField.clipsToBounds = true
        
        signInButton.layer.cornerRadius = height
        signInButton.clipsToBounds = true
        
        imageView.layer.cornerRadius = height
        imageView.clipsToBounds = true
        
        userNameTextField.text = ""
        passwordTextField.text = ""
        
        setSpinner(isSpinning: false)
    }
    
    private func setSpinner(isSpinning: Bool){
        if isSpinning{
            spinnerView.startAnimating()
            downloading = true
        }else{
            spinnerView.stopAnimating()
            downloading = false
        }
    }
    
    @objc private func dismissKeyboard() {
        view.endEditing(true)
    }
}


